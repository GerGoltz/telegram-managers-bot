"""Middleware configuration"""
from aiogram import Dispatcher

from .throttling import ThrottlingMiddleware


def setup(dp: Dispatcher):
    """Setup throttling"""
    dp.middleware.setup(ThrottlingMiddleware())
