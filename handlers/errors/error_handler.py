"""Error handler"""
from aiogram.utils.exceptions import (Unauthorized,
                                      InvalidQueryID,
                                      TelegramAPIError,
                                      CantDemoteChatCreator,
                                      MessageNotModified,
                                      MessageToDeleteNotFound,
                                      MessageTextIsEmpty,
                                      RetryAfter,
                                      CantParseEntities,
                                      MessageCantBeDeleted)
from loguru import logger

from utils.loader import dp


@dp.errors_handler()
async def errors_handler(update, exception):
    """
    Exceptions handler. Catches all exceptions within task factory tasks.
    :param dispatcher:
    :param update:
    :param exception:
    :return: stdout logging
    """

    if isinstance(exception, CantDemoteChatCreator):
        logger.debug('Can\'t demote chat creator')

    elif isinstance(exception, MessageNotModified):
        logger.debug('Message is not modified')

    elif isinstance(exception, MessageCantBeDeleted):
        logger.debug('Message cant be deleted')

    elif isinstance(exception, MessageToDeleteNotFound):
        logger.debug('Message to delete not found')

    elif isinstance(exception, MessageTextIsEmpty):
        logger.debug('MessageTextIsEmpty')

    elif isinstance(exception, Unauthorized):
        logger.info(f'Unauthorized: {exception}')

    elif isinstance(exception, InvalidQueryID):
        logger.exception(f'InvalidQueryID: {exception} \n'
                         f'Update: {update}')

    elif isinstance(exception, TelegramAPIError):
        logger.exception(f'TelegramAPIError: {exception} \n'
                         f'Update: {update}')

    elif isinstance(exception, RetryAfter):
        logger.exception(f'RetryAfter: {exception} \n'
                         f'Update: {update}')

    elif isinstance(exception, CantParseEntities):
        logger.exception(f'CantParseEntities: {exception} \n'
                         f'Update: {update}')

    else:
        logger.exception(f'Update: {update} \n'
                         f'{exception}')
        return

    return True
