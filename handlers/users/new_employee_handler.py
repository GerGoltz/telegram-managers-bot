"""New employee case handlers"""
import json
from datetime import datetime

from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text, Regexp
from aiogram.types import (Message, InlineKeyboardMarkup,
                           InlineKeyboardButton, CallbackQuery)
from loguru import logger

from handlers.users.utils import (verbose_result, respond_if_error,
                                  generate_feedback_kb)
from models.tasks import NewEmployeeTask
from models.users import TaskReceiver
from utils.keyboards import cancel_keyboard, choices_keyboard
from utils.loader import dp, bot
from utils.states import NewEmployeeApplication
from utils.texts import (NEW_EMPLOYEE_TEXT, NEW_EMPLOYEE_DATE_TEXT,
                         NEW_EMPLOYEE_TIME_TEXT, NEW_EMPLOYEE_POSITION_TEXT,
                         NEW_EMPLOYEE_MENTOR_TEXT,
                         NEW_EMPLOYEE_REQUIRED_TEXT, NEW_EMPLOYEE_STATUS_TEXT,
                         FINISH_TEXT, NEW_EMPLOYEE_LITER, ERROR_HINT_TEXT,
                         NEW_EMPLOYEE_STATUS, NEW_EMPLOYEE_FULLNAME_TEXT)
from utils.throttling import rate_limit

REGEXP_EXPRESSION_128 = r'^.{1,128}$'


#  Step: 1 - start
@rate_limit(2, 'new_employee_start')
@dp.message_handler(Text(equals=NEW_EMPLOYEE_TEXT))
async def new_employee_start(message: Message):
    """New employee order : step 1"""
    await message.answer(NEW_EMPLOYEE_FULLNAME_TEXT,
                         reply_markup=cancel_keyboard)
    await NewEmployeeApplication.first()


#  Step: 2 - EMPLOYEE FULLNAME
@rate_limit(2, 'new_employee_start')
@dp.message_handler(Regexp(REGEXP_EXPRESSION_128),
                    state=NewEmployeeApplication.EmployeeFullname)
async def new_employee_start(message: Message, state: FSMContext):
    """New employee order : step 1"""
    await state.update_data({'employee_fullname': message.text})
    await NewEmployeeApplication.next()
    await message.answer(NEW_EMPLOYEE_DATE_TEXT)
    logger.info(f'{message.from_user.first_name}: '
                f'new employee fullname: {message.text}')


# Step: 2 - DATE
@rate_limit(4, 'new_employee_date')
@dp.message_handler(state=NewEmployeeApplication.Date)
async def new_employee_date(message: Message, state: FSMContext):
    """New employee order : step 2"""
    _text = message.text
    try:
        datetime.strptime(_text, '%d.%m.%Y')
    except ValueError:
        await message.answer(ERROR_HINT_TEXT
                             .format(NewEmployeeApplication.Date.hint))
        logger.info(f'{message.from_user.first_name}: '
                    f'new employee error date: {message.text}')
    else:
        await state.update_data({'date': _text})
        await message.answer(NEW_EMPLOYEE_TIME_TEXT)
        await NewEmployeeApplication.next()
        logger.info(f'{message.from_user.first_name}: '
                    f'new employee date: {message.text}')


# Step: 3 - TIME
@rate_limit(4, 'new_employee_time')
@dp.message_handler(state=NewEmployeeApplication.Time)
async def new_employee_time(message: Message, state: FSMContext):
    """New employee order : step 3"""
    _text = message.text
    try:
        datetime.strptime(_text, '%H:%M')
    except ValueError:
        await message.answer(ERROR_HINT_TEXT
                             .format(NewEmployeeApplication.Time.hint))
        logger.info(f'{message.from_user.first_name}: '
                    f'new employee error time: {message.text}')
    else:
        await state.update_data({'time': _text})
        await NewEmployeeApplication.next()
        await message.answer(NEW_EMPLOYEE_POSITION_TEXT)
        logger.info(f'{message.from_user.first_name}: '
                    f'new employee time: {message.text}')


# Step: 4 - POSITION
@dp.message_handler(Regexp(REGEXP_EXPRESSION_128),
                    state=NewEmployeeApplication.Position)
async def new_employee_position(message: Message, state: FSMContext):
    """New employee order : step 4"""
    await state.update_data({'position': message.text})
    await NewEmployeeApplication.next()
    await message.answer(NEW_EMPLOYEE_MENTOR_TEXT)
    logger.info(f'{message.from_user.first_name}: '
                f'new employee position: {message.text}')


# Step: 5 - MENTOR
@dp.message_handler(Regexp(REGEXP_EXPRESSION_128),
                    state=NewEmployeeApplication.Mentor)
async def new_employee_mentor(message: Message, state: FSMContext):
    """New employee order : step 5"""
    await state.update_data({'mentor': message.text})
    await NewEmployeeApplication.next()
    await message.answer(NEW_EMPLOYEE_REQUIRED_TEXT)
    logger.info(f'{message.from_user.first_name}: '
                f'new employee mentor: {message.text}')


# Step: 6 - REQUIRED
@dp.message_handler(state=NewEmployeeApplication.Required)
async def new_employee_required(message: Message, state: FSMContext):
    """New employee order : step 6"""
    _text = message.text
    await state.update_data({'required': _text})
    await NewEmployeeApplication.next()
    await message.answer(NEW_EMPLOYEE_STATUS_TEXT,
                         reply_markup=InlineKeyboardMarkup(
                             inline_keyboard=[
                                 [InlineKeyboardButton(text=NEW_EMPLOYEE_STATUS['test'],
                                                       callback_data='test')],
                                 [InlineKeyboardButton(text=NEW_EMPLOYEE_STATUS['state'],
                                                       callback_data='state')]
                             ]
                         ))
    logger.info(f'{message.from_user.first_name}: '
                f'new employee required: {_text}')


# Step: 7 - STATUS --> Check
@rate_limit(20, 'new_employee_check')
@dp.callback_query_handler(Text(equals=NEW_EMPLOYEE_STATUS.keys()),
                           state=NewEmployeeApplication.Status)
async def new_employee_check(call: CallbackQuery, state: FSMContext):
    """New employee order : check"""
    await call.message.delete_reply_markup()
    await state.update_data({'status': NEW_EMPLOYEE_STATUS[call.data]})
    result = await state.get_data()
    await call.message.answer(verbose_result('new_employee', result),
                              reply_markup=InlineKeyboardMarkup(
                                  inline_keyboard=[
                                      [InlineKeyboardButton(
                                          text='ok', callback_data='ok'
                                      )]
                                  ]
                              ))
    logger.info(f'{call.from_user.first_name}: '
                f'new employee status: {NEW_EMPLOYEE_STATUS[call.data]}\n'
                f'{call.from_user.first_name}: checking ...')


# Step: Finish
@rate_limit(10, 'new_employee_finish')
@dp.callback_query_handler(Text(equals='ok'),
                           state=NewEmployeeApplication.Status)
async def new_employee_finish(call: CallbackQuery, state: FSMContext):
    """New employee order : finish"""
    logger.info(f'{call.from_user.first_name}: '
                'new employee finished')
    result = await state.get_data()
    result['date'] = datetime.strptime(
        f"{result['date']} {result.pop('time')}",
        '%d.%m.%Y %H:%M'
    )
    task = await NewEmployeeTask.create(
        from_user_id=call.from_user.id,
        from_user_name=call.from_user.first_name,
        **result
    )
    await call.message.delete_reply_markup()
    await state.finish()
    task_id = f'{NEW_EMPLOYEE_LITER}{task.id}'
    await call.message.answer(
        FINISH_TEXT.format(task_id),
        reply_markup=choices_keyboard
    )
    messages = []
    result['date'] = result['date'].strftime('%d.%m.%Y %H:%M')
    feedback_kb = generate_feedback_kb(task_id)
    for proj_manager in await TaskReceiver.all().values_list('user_id',
                                                             flat=True):
        message = await bot.send_message(
            chat_id=proj_manager,
            text=(verbose_result('new_employee', result)
                  + f'\nFROM {call.from_user.first_name}'),
            reply_markup=feedback_kb
        )
        messages.append((proj_manager, message.message_id))
    await dp.storage._redis.set(f'task:{task_id}', json.dumps(messages))


# Validation errors
@rate_limit(4, 'new_employee_error')
@dp.message_handler(state=NewEmployeeApplication.EmployeeFullname)
@dp.message_handler(state=NewEmployeeApplication.Position)
@dp.message_handler(state=NewEmployeeApplication.Mentor)
@dp.message_handler(state=NewEmployeeApplication.Status)
async def new_employee_error(message: Message, state: FSMContext):
    """Input error handling"""
    await respond_if_error(message, state, NewEmployeeApplication)
