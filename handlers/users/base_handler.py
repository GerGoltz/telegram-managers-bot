"""Reaction on /start command"""

import tortoise
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.builtin import CommandStart, Text, Regexp
from aiogram.types import Message, CallbackQuery
from loguru import logger
from tortoise.timezone import now

from config import SECRET_PM_WORD
from handlers.users.utils import delete_pm_keyboards_after_done
from models.mappings import TASK_LITER_MAP
from models.users import TaskReceiver
from utils.keyboards import choices_keyboard
from utils.loader import dp, bot
from utils.texts import (START_BOT_TEXT, CANCEL_TEXT,
                         CHOICE_ORDER_TEXT, SECRET_RESPONSE,
                         ORDER_CLOSE_TEXT, SECRET_DOUBLE_TEXT,
                         GOOD_JOB_TEXT, NEW_EMPLOYEE_LITER,
                         TECH_HELP_LITER, DISMISSAL_LITER)
from utils.throttling import rate_limit

ORDER_DONE_REGEXP = (f'^[{NEW_EMPLOYEE_LITER}'
                     f'{TECH_HELP_LITER}{DISMISSAL_LITER}][0-9]+$')


@rate_limit(5, 'start')
@dp.message_handler(CommandStart())
async def bot_start(message: Message):
    """Command /start handling"""
    logger.info(f'/start by {message.from_user.first_name}. '
                f'USER_ID: {message.from_user.id}')
    await message.answer(START_BOT_TEXT, reply_markup=choices_keyboard)


@dp.message_handler(Text(equals=CANCEL_TEXT), state='*')
async def cancel(message: Message, state: FSMContext):
    """Cancel cases handling"""
    logger.info(f'cancel by {message.from_user.first_name}. '
                f'USER_ID: {message.from_user.id}')
    await message.answer(CHOICE_ORDER_TEXT, reply_markup=choices_keyboard)
    await state.finish()


@dp.message_handler(Text(equals=SECRET_PM_WORD))
async def secret_pm(message: Message):
    """Secret case handling"""
    try:
        await TaskReceiver.create(
            user_id=message.from_user.id,
            user_name=message.from_user.first_name)
        logger.warning('New task receiver!\n'
                       f'{message.from_user.first_name}. \n'
                       f'USER_ID: {message.from_user.id}')
        await message.answer(SECRET_RESPONSE)
    except tortoise.exceptions.IntegrityError:
        await message.answer(SECRET_DOUBLE_TEXT)


@dp.callback_query_handler(Regexp(ORDER_DONE_REGEXP),
                           state='*')
async def new_employee_feedback(call: CallbackQuery):
    """New employee order : finish"""
    await delete_pm_keyboards_after_done(f'task:{call.data}')
    task = await TASK_LITER_MAP[call.data[0]].get(id=call.data[1:])
    task.is_done = True
    task.successor = await TaskReceiver.get(user_id=call.from_user.id)  # optimize
    task.closed_at = now()
    await task.save()
    await call.message.answer(GOOD_JOB_TEXT)
    await bot.send_message(chat_id=task.from_user_id,
                           text=ORDER_CLOSE_TEXT.format(call.data))
