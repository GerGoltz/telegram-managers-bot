"""Tech help case handlers"""
import json
import os
from string import digits

from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Regexp, Text
from aiogram.types import (Message, InlineKeyboardMarkup,
                           InlineKeyboardButton, CallbackQuery,
                           ContentType)
from loguru import logger
from tortoise.timezone import now

from handlers.users.utils import (verbose_result, generate_feedback_kb,
                                  respond_if_error,
                                  delete_pm_keyboards_after_done)
from models.tasks import TechHelpTask
from models.users import TaskReceiver
from utils.keyboards import cancel_keyboard, choices_keyboard
from utils.loader import dp, bot
from utils.states import TechHelpApplication
from utils.texts import (TECH_HELP_TEXT, TECH_HELP_INITIATOR_FULLNAME_TEXT,
                         TECH_HELP_TASK_TYPE_TEXT,
                         TECH_HELP_TASK_TYPE_MAPPING,
                         NO_FILE_CASE, TECH_HELP_DESCRIPTION_TEXT,
                         DESCRIPTION_NEXT_STATE_MAP, TECH_HELP_FILE_TEXT,
                         FINISH_TEXT, TECH_HELP_LITER, FILE_CAPTION,
                         FILE_DESTINATION, TECH_HELP_FILE_READY_TEXT,
                         TECH_HELP_FILE_RECEIVE_TEXT, TECH_TASK_DONE_CAPTION,
                         GOOD_JOB_TEXT, ORDER_CLOSE_TEXT)
from utils.throttling import rate_limit

TASK_TYPE_KB = InlineKeyboardMarkup(inline_keyboard=[
    [InlineKeyboardButton(text=value, callback_data=key)]
    for key, value in TECH_HELP_TASK_TYPE_MAPPING.items()
])


def generate_tech_help_ready_kb(task_id) -> InlineKeyboardMarkup:
    """Generate tech help ready kb to be sent to managers"""
    return InlineKeyboardMarkup(inline_keyboard=[
        [InlineKeyboardButton(text=TECH_HELP_FILE_READY_TEXT,
                              callback_data=f'TODO:{task_id}')]
    ])


async def file_receive_handle(state: FSMContext,
                              file_id: str,
                              message: Message):
    """Repetitive part of file handlers (State: 5)"""
    result = await state.get_data()
    result['task_type'] = TECH_HELP_TASK_TYPE_MAPPING[result['task_type']]
    result['source_path'] = FILE_CAPTION
    del result['file_name']
    _type = result.pop('type')
    _payload = {_type: file_id,
                'caption': verbose_result('tech_help', result),
                'reply_markup': InlineKeyboardMarkup(
                    inline_keyboard=[
                        [InlineKeyboardButton(
                            text='ok', callback_data='ok'
                        )]
                    ]
                )}
    await getattr(message, f'answer_{_type}')(**_payload)
    await TechHelpApplication.next()


async def file_send_handle(state: FSMContext,
                           file_id: str,
                           file_name: str,
                           message: Message,
                           _type: str):
    """Repetitive part of file handlers (State: Done)"""
    result = await state.get_data()
    task = await TechHelpTask.get(id=result['task_id'])
    dir_path = FILE_DESTINATION.format(task.id)
    _path = f'{dir_path}/{file_name}'
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    await bot.download_file_by_id(file_id=file_id,
                                  destination=_path)
    task.solution_path = _path
    task.is_done = True
    task.successor = await TaskReceiver.get(user_id=message.from_user.id)
    task.closed_at = now()
    await task.save()
    _payload = {'chat_id': task.from_user_id,
                'caption': ORDER_CLOSE_TEXT.format(
                    f'{TECH_HELP_LITER}{task.id}'
                ) + '\n' + TECH_TASK_DONE_CAPTION,
                _type: file_id}
    await state.finish()
    await delete_pm_keyboards_after_done(f'task:{TECH_HELP_LITER}{task.id}')
    await message.answer(GOOD_JOB_TEXT)
    await getattr(bot, f'send_{_type}')(**_payload)


#  Step: 1 - Start
@rate_limit(2, 'tech_help_start')
@dp.message_handler(Text(equals=TECH_HELP_TEXT))
async def tech_help_start(message: Message):
    """Tech help order : step 1"""
    await message.answer(TECH_HELP_INITIATOR_FULLNAME_TEXT,
                         reply_markup=cancel_keyboard)
    await TechHelpApplication.first()


#  Step: 2 - INITIATOR FULLNAME
@dp.message_handler(Regexp(r'^.{1,128}$'),
                    state=TechHelpApplication.InitiatorFullname)
async def tech_help_full_name(message: Message, state: FSMContext):
    """Tech help order : step 2"""
    await message.answer(TECH_HELP_TASK_TYPE_TEXT,
                         reply_markup=TASK_TYPE_KB)
    await state.set_data({'initiator_fullname': message.text})
    await TechHelpApplication.next()
    logger.info(f'{message.from_user.first_name}: '
                f'tech help fullname: {message.text}')


#  Step: 3 - TASK TYPE
@dp.callback_query_handler(state=TechHelpApplication.TaskType)
async def tech_help_task_type(call: CallbackQuery, state: FSMContext):
    """Tech help order : step 3"""
    await call.message.answer(TECH_HELP_DESCRIPTION_TEXT)
    await state.update_data({'task_type': call.data})
    await TechHelpApplication.next()
    logger.info(f'{call.from_user.first_name}: '
                'tech help task type: '
                f'{TECH_HELP_TASK_TYPE_MAPPING[call.data]}')


#  Step: 4 - DESCRIPTION --> check or file
@dp.message_handler(state=TechHelpApplication.Description)
async def tech_help_description(message: Message, state: FSMContext):
    """Tech help order : step 4"""
    await state.update_data({'description': message.text})
    logger.info(f'{message.from_user.first_name}: '
                f'tech help description: {message.text}')
    result = await state.get_data()
    await getattr(TechHelpApplication,
                  DESCRIPTION_NEXT_STATE_MAP[result['task_type']]).set()
    if (result['task_type']) in NO_FILE_CASE:
        result['task_type'] = TECH_HELP_TASK_TYPE_MAPPING[result['task_type']]
        await message.answer(verbose_result('tech_help', result),
                             reply_markup=InlineKeyboardMarkup(
                                 inline_keyboard=[
                                     [InlineKeyboardButton(
                                         text='ok', callback_data='ok'
                                     )]
                                 ]
                             ))
    else:
        await message.answer(TECH_HELP_FILE_TEXT)


#  Optional[Step]: 5 - FILE (link) --> check
@dp.message_handler(state=TechHelpApplication.File,
                    content_types=ContentType.TEXT)
async def tech_help_file(message: Message, state: FSMContext):
    """Tech help order : step 5"""
    await state.update_data({'source_path': f'LINK!:{message.text}'})
    logger.info(f'{message.from_user.first_name}: '
                f'tech help link: {message.text}')
    result = await state.get_data()
    await TechHelpApplication.next()
    result['task_type'] = TECH_HELP_TASK_TYPE_MAPPING[result['task_type']]
    result['source_path'] = message.text
    await message.answer(verbose_result('tech_help', result),
                         reply_markup=InlineKeyboardMarkup(
                             inline_keyboard=[
                                 [InlineKeyboardButton(
                                     text='ok', callback_data='ok'
                                 )]
                             ]
                         ))


#  Optional[Step]: 5 - FILE (document) --> check
@dp.message_handler(state=TechHelpApplication.File,
                    content_types=ContentType.DOCUMENT)
async def tech_help_document(message: Message, state: FSMContext):
    """Tech help order : step 5"""
    file_id = message.document.file_id
    file_name = message.document.file_name
    await state.update_data({'source_path': file_id,
                             'file_name': file_name,
                             'type': 'document'})
    logger.info(f'{message.from_user.first_name}: '
                f'tech help document: {message.caption}')
    await file_receive_handle(state, file_id, message)


#  Optional[Step]: 5 - FILE (photo) --> check
@dp.message_handler(state=TechHelpApplication.File,
                    content_types=ContentType.PHOTO)
async def tech_help_photo(message: Message, state: FSMContext):
    """Tech help order : step 5"""
    file_id = message.photo[-1].file_id
    file_name = 'photo.jpg'
    await state.update_data({'source_path': file_id,
                             'file_name': file_name,
                             'type': 'photo'})
    logger.info(f'{message.from_user.first_name}: '
                f'tech help photo: {message.caption}')
    await file_receive_handle(state, file_id, message)


#  Optional[Step]: 5 - FILE (video) --> check
@dp.message_handler(state=TechHelpApplication.File,
                    content_types=ContentType.VIDEO)
async def tech_help_file(message: Message, state: FSMContext):
    """Tech help order : step 5"""
    file_id = message.video.file_id
    file_name = message.video.file_name
    await state.update_data({'source_path': file_id,
                             'file_name': file_name,
                             'type': 'video'})
    logger.info(f'{message.from_user.first_name}: '
                f'tech help video: {message.caption}')
    await file_receive_handle(state, file_id, message)


# Step: Finish
@rate_limit(10, 'tech_help_finish')
@dp.callback_query_handler(Text(equals='ok'),
                           state=TechHelpApplication.Check)
async def tech_help_finish(call: CallbackQuery, state: FSMContext):
    """Tech help order : finish"""
    logger.info(f'{call.from_user.first_name}: tech help finish')
    result = await state.get_data()
    result['task_type'] = TECH_HELP_TASK_TYPE_MAPPING[result['task_type']]
    link = False
    if (result.get('source_path')
            and result['source_path'].startswith('LINK!:')):
        result['source_path'] = result['source_path'][6:]
        link = True
    try:
        file_name = result.pop('file_name')
        _type = result.pop('type')
    except KeyError:
        file_name, _type = 'no file', 'no type'
    task = await TechHelpTask.create(
        from_user_id=call.from_user.id,
        from_user_name=call.from_user.first_name,
        **result
    )
    await call.message.delete_reply_markup()
    await state.finish()
    task_id = f'{TECH_HELP_LITER}{task.id}'
    await call.message.answer(
        FINISH_TEXT.format(task_id),
        reply_markup=choices_keyboard
    )
    messages = []
    if not link and result.get('source_path'):
        _path = FILE_DESTINATION.format(task.id)
        os.makedirs(_path)
        todo_keyboard = generate_tech_help_ready_kb(task_id)
        file_id = result['source_path']
        await bot.download_file_by_id(file_id=file_id,
                                      destination=f'{_path}/{file_name}')
        task.source_path = f'{_path}/{file_name}'
        await task.save()
        del result['source_path']
        _payload = {'caption': (verbose_result('tech_help', result)
                                + f'\nFROM {call.from_user.first_name}'),
                    'reply_markup': todo_keyboard,
                    _type: file_id}
        for proj_manager in await TaskReceiver.all().values_list('user_id',
                                                                 flat=True):
            message = await getattr(bot, f'send_{_type}')(
                chat_id=proj_manager, **_payload
            )
            messages.append((proj_manager, message.message_id))
    else:
        _payload = {'text': (verbose_result('tech_help', result)
                             + f'\nFROM {call.from_user.first_name}'),
                    'reply_markup': generate_feedback_kb(task_id)
                    if not result.get('source_path')
                    else generate_tech_help_ready_kb(task_id)}
        for proj_manager in await TaskReceiver.all().values_list('user_id',
                                                                 flat=True):
            message = await bot.send_message(chat_id=proj_manager, **_payload)
            messages.append((proj_manager, message.message_id))
    await dp.storage._redis.set(f'task:{task_id}', json.dumps(messages))


# Step: Task is Done
@dp.callback_query_handler(Regexp(f'^TODO:{TECH_HELP_LITER}[0-9]+$'))
async def tech_help_done(call: CallbackQuery, state: FSMContext):
    """Tech help order : done"""
    await call.message.delete_reply_markup()
    task_id = int(''.join(x for x in call.data if x in digits))
    await call.message.answer(TECH_HELP_FILE_RECEIVE_TEXT)
    await TechHelpApplication.Active.set()
    await state.set_data({'task_id': task_id})
    logger.info(f'{call.from_user.first_name}: '
                f'tech help order {TECH_HELP_LITER}{task_id} done. '
                f'waiting for solution ...')


#  Optional[Step]: Done - FILE (text)
@dp.message_handler(state=TechHelpApplication.Active,
                    content_types=ContentType.TEXT)
async def tech_help_document(message: Message, state: FSMContext):
    """Tech help order : step 5"""
    logger.info(f'{message.from_user.first_name}: '
                f'tech help text: {message.text}')
    result = await state.get_data()
    task = await TechHelpTask.get(id=result['task_id'])
    task.solution_path = message.text
    task.is_done = True
    task.successor = await TaskReceiver.get(user_id=message.from_user.id)
    task.closed_at = now()
    await task.save()
    _payload = {'chat_id': task.from_user_id,
                'text': ORDER_CLOSE_TEXT.format(
                    f'{TECH_HELP_LITER}{task.id}'
                ) + '\n' + message.text}
    await state.finish()
    await delete_pm_keyboards_after_done(f'task:{TECH_HELP_LITER}{task.id}')
    await message.answer(GOOD_JOB_TEXT)
    await bot.send_message(**_payload)


#  Optional[Step]: Done - FILE (document)
@dp.message_handler(state=TechHelpApplication.Active,
                    content_types=ContentType.DOCUMENT)
async def tech_help_document(message: Message, state: FSMContext):
    """Tech help order : step 5"""
    file_id = message.document.file_id
    file_name = 'done_' + message.document.file_name
    await state.update_data({'file_id': file_id,
                             'type': 'document'})
    logger.info(f'{message.from_user.first_name}: '
                f'tech help document: {message.caption}')
    await file_send_handle(state, file_id, file_name, message,
                           _type='document')


#  Optional[Step]: Done - FILE (photo)
@dp.message_handler(state=TechHelpApplication.Active,
                    content_types=ContentType.PHOTO)
async def tech_help_photo(message: Message, state: FSMContext):
    """Tech help order : step 5"""
    file_id = message.photo[-1].file_id
    file_name = 'done_photo.jpg'
    await state.update_data({'file_id': file_id,
                             'type': 'photo'})
    logger.info(f'{message.from_user.first_name}: '
                f'tech help done - photo: {message.caption}')
    await file_send_handle(state, file_id, file_name, message,
                           _type='photo')


#  Optional[Step]: Done - FILE (video)
@dp.message_handler(state=TechHelpApplication.Active,
                    content_types=ContentType.VIDEO)
async def tech_help_file(message: Message, state: FSMContext):
    """Tech help order : step 5"""
    file_id = message.video.file_id
    file_name = 'done_' + message.video.file_name
    logger.info(f'{message.from_user.first_name}: '
                f'tech help done - video: {message.caption}')
    await file_send_handle(state, file_id, file_name, message,
                           _type='video')


# Validation errors
@rate_limit(4, 'tech_help_error')
@dp.message_handler(state=TechHelpApplication.InitiatorFullname)
@dp.message_handler(state=TechHelpApplication.TaskType)
@dp.message_handler(state=TechHelpApplication.Check)
@dp.message_handler(state=TechHelpApplication.File)
async def dismissal_error(message: Message, state: FSMContext):
    """Input error handling"""
    await respond_if_error(message, state, TechHelpApplication)
