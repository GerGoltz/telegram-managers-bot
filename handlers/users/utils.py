"""Utils common used"""
import json

from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from aiogram.utils.exceptions import MessageNotModified
from loguru import logger

from utils.loader import dp, bot
from utils.texts import (NEW_EMPLOYEE_VERBOSE_MAPPING,
                         NEW_EMPLOYEE_TEXT, DISMISSAL_TEXT,
                         SUCCESS_TEXT, DISMISSAL_VERBOSE_MAPPING,
                         ERROR_HINT_TEXT, TECH_HELP_TEXT,
                         TECH_HELP_VERBOSE_MAPPING)

VERBOSE_MAPPING = {
    'new_employee': NEW_EMPLOYEE_VERBOSE_MAPPING,
    'dismissal': DISMISSAL_VERBOSE_MAPPING,
    'tech_help': TECH_HELP_VERBOSE_MAPPING
}
TEXT_MAPPING = {
    'new_employee': NEW_EMPLOYEE_TEXT,
    'dismissal': DISMISSAL_TEXT,
    'tech_help': TECH_HELP_TEXT
}


def verbose_result(task: str, data: dict) -> str:
    """Verbose given data to confirm order"""
    result = '\n'.join(f'<b>{VERBOSE_MAPPING[task][key]}</b>: {value}'
                       for key, value in data.items())
    return f'<b>{TEXT_MAPPING[task]}</b>\n\n{result}'


def generate_feedback_kb(task_id) -> InlineKeyboardMarkup:
    """Generate feedback kb to be sent to managers"""
    return InlineKeyboardMarkup(inline_keyboard=[
        [InlineKeyboardButton(text=SUCCESS_TEXT, callback_data=task_id)]
    ])


async def delete_pm_keyboards_after_done(redis_key: str):
    """Delete inline keyboards from all proj managers because task is done"""
    messages: list[tuple] = json.loads(
        await dp.storage._redis.get(redis_key)
    )
    for chat_id, message_id in messages:
        try:
            await bot.edit_message_reply_markup(message_id=message_id,
                                                chat_id=chat_id)
        except MessageNotModified:
            continue
    await dp.storage._redis.delete(redis_key)


async def respond_if_error(message, state, group):
    """Respond if errors"""
    _state = (await state.get_state()).split(':')[1]
    logger.warning('Invalid data for dismissal task: \n'
                   f'{message.from_user.first_name}: {_state}: '
                   f'{message.text}')
    await message.answer(ERROR_HINT_TEXT.format(
        getattr(group, _state).hint)
    )
