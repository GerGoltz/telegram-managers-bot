"""Register all dispatcher handlers"""
from .base_handler import dp
from .dismissal_handler import dp
from .new_employee_handler import dp
from .tech_help_handler import dp

__all__ = ['dp']
