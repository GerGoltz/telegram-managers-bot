"""Dismissal case handlers"""
import json
from datetime import datetime

from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text, Regexp
from aiogram.types import (Message, CallbackQuery,
                           InlineKeyboardMarkup, InlineKeyboardButton)
from loguru import logger

from handlers.users.utils import (verbose_result, generate_feedback_kb,
                                  respond_if_error)
from models.tasks import DismissalTask
from models.users import TaskReceiver
from utils.keyboards import cancel_keyboard, choices_keyboard
from utils.loader import dp, bot
from utils.states import DismissalApplication
from utils.texts import (DISMISSAL_TEXT, DISMISSAL_INITIATOR_FULLNAME_TEXT,
                         ERROR_HINT_TEXT, DISMISSAL_DATE_TEXT,
                         DISMISSAL_DISMISSED_FULLNAME_TEXT,
                         DISMISSAL_EQUIPMENT_TEXT, DISMISSAL_LITER,
                         FINISH_TEXT)
from utils.throttling import rate_limit


#  Step: 1 - start
@rate_limit(3, 'dismissal_start')
@dp.message_handler(Text(equals=DISMISSAL_TEXT))
async def dismissal_start(message: Message):
    """Dismissal order : step 1"""
    await message.answer(DISMISSAL_INITIATOR_FULLNAME_TEXT,
                         reply_markup=cancel_keyboard)
    await DismissalApplication.first()


#  Step: 2 - INITIATOR FULLNAME
@dp.message_handler(Regexp(r'^.{1,128}$'),
                    state=DismissalApplication.InitiatorFullname)
async def dismissal_initiator(message: Message, state: FSMContext):
    """Dismissal order : step 2"""
    await state.set_data({'initiator_fullname': message.text})
    await DismissalApplication.next()
    await message.answer(DISMISSAL_DATE_TEXT)
    logger.info(f'{message.from_user.first_name}: '
                f'dismissal initiator fullname: {message.text}')


# Step: 3 - DATE
@rate_limit(4, 'dismissal_date')
@dp.message_handler(state=DismissalApplication.Date)
async def dismissal_date(message: Message, state: FSMContext):
    """New employee order : step 3"""
    _text = message.text
    try:
        datetime.strptime(_text, '%d.%m.%Y')
    except ValueError:
        await message.answer(ERROR_HINT_TEXT
                             .format(DismissalApplication.Date.hint))
        logger.info(f'{message.from_user.first_name}: '
                    f'dismissal date error: {message.text}')
    else:
        await state.update_data({'date': _text})
        await DismissalApplication.next()
        await message.answer(DISMISSAL_DISMISSED_FULLNAME_TEXT)
        logger.info(f'{message.from_user.first_name}: '
                    f'dismissal date: {message.text}')


#  Step: 4 - DISMISSED FULLNAME
@dp.message_handler(Regexp(r'^.{1,128}$'),
                    state=DismissalApplication.DismissedFullname)
async def dismissal_dismissed(message: Message, state: FSMContext):
    """Dismissal order : step 4"""
    await state.update_data({'dismissed_fullname': message.text})
    await DismissalApplication.next()
    await message.answer(DISMISSAL_EQUIPMENT_TEXT)
    logger.info(f'{message.from_user.first_name}: '
                f'dismissal dismissed fullname: {message.text}')


#  Step: 5 - EQUIPMENT --> Check
@rate_limit(10, 'dismissal_check')
@dp.message_handler(Regexp(r'^.{1,256}$'),
                    state=DismissalApplication.Equipment)
async def dismissal_equipment(message: Message, state: FSMContext):
    """Dismissal order : check"""
    await state.update_data({'equipment': message.text})
    await DismissalApplication.next()
    result = await state.get_data()
    await message.answer(verbose_result('dismissal', result),
                         reply_markup=InlineKeyboardMarkup(
                             inline_keyboard=[
                                 [InlineKeyboardButton(
                                     text='ok', callback_data='ok'
                                 )]
                             ]
                         ))
    logger.info(f'{message.from_user.first_name}: '
                f'dismissal equipment: {message.text}\n'
                f'{message.from_user.first_name}: checking ...')


# Step: Finish
@rate_limit(10, 'dismissal_finish')
@dp.callback_query_handler(Text(equals='ok'),
                           state=DismissalApplication.Check)
async def dismissal_finish(call: CallbackQuery, state: FSMContext):
    """Dismissal order : finish"""
    logger.info(f'{call.from_user.first_name}: dismissal finish')
    result = await state.get_data()
    result['date'] = datetime.strptime(result['date'], '%d.%m.%Y')
    task = await DismissalTask.create(
        from_user_id=call.from_user.id,
        from_user_name=call.from_user.first_name,
        **result
    )
    await call.message.delete_reply_markup()
    await state.finish()
    task_id = f'{DISMISSAL_LITER}{task.id}'
    await call.message.answer(
        FINISH_TEXT.format(task_id),
        reply_markup=choices_keyboard
    )
    messages = []
    result['date'] = result['date'].strftime('%d.%m.%Y')
    feedback_kb = generate_feedback_kb(task_id)
    for proj_manager in await TaskReceiver.all().values_list('user_id',
                                                             flat=True):
        message = await bot.send_message(
            chat_id=proj_manager,
            text=(verbose_result('dismissal', result)
                  + f'\nFROM {call.from_user.first_name}'),
            reply_markup=feedback_kb
        )
        messages.append((proj_manager, message.message_id))
    await dp.storage._redis.set(f'task:{task_id}', json.dumps(messages))


# Validation errors
@rate_limit(4, 'dismissal_error')
@dp.message_handler(state=DismissalApplication.InitiatorFullname)
@dp.message_handler(state=DismissalApplication.DismissedFullname)
@dp.message_handler(state=DismissalApplication.Equipment)
@dp.message_handler(state=DismissalApplication.Check)
async def dismissal_error(message: Message, state: FSMContext):
    """Input error handling"""
    await respond_if_error(message, state, DismissalApplication)
