"""Main app launcher"""
import os

import aioredis
from fastapi import Request, Form, HTTPException
from loguru import logger
from starlette.responses import HTMLResponse, RedirectResponse, FileResponse
from starlette.status import HTTP_302_FOUND
from starlette.templating import Jinja2Templates
from tortoise.timezone import now

from models.tasks import TechHelpTask, NewEmployeeTask, DismissalTask
from utils.app_config import get_application
from utils.credentials import REDIS_HOST, REDIS_PORT
from utils.mappings import TASK_MAP, TASK_VERBOSE_MAP

app = get_application()
templates = Jinja2Templates(directory='templates')


@app.on_event('startup')
async def startup_event():
    """On startup event"""
    app.state.redis = await aioredis.create_redis(
        f'redis://{REDIS_HOST}:{REDIS_PORT}/0',
        encoding='utf-8'
    )


@app.on_event('shutdown')
async def shutdown_event():
    """On shutdown event"""
    app.state.redis.close()
    await app.state.redis.wait_closed()


@app.get('/')
async def hello_there(request: Request):
    """Redirect manager"""
    if request.session.get('access'):
        return RedirectResponse('/today', status_code=HTTP_302_FOUND)
    return RedirectResponse('/login', status_code=HTTP_302_FOUND)


@app.get('/login', response_class=HTMLResponse)
async def login_page(request: Request):
    """Login"""
    return templates.TemplateResponse('login_page.html', {'request': request,
                                                          'error': None})


@app.get('/logout', response_class=HTMLResponse)
async def logout(request: Request):
    """Logout"""
    logger.info(f'{request.session["username"]} has just logged out')
    request.session['access'] = False
    return RedirectResponse('/login', status_code=HTTP_302_FOUND)


def is_valid(session: dict, username: str, password: str) -> bool:
    """Check if login data is valid"""
    if username == 'admin' and password == 'secret_password229':
        session['access'] = True
        session['username'] = username
        return True
    return False


@app.post('/login', response_class=RedirectResponse)
async def login_check(request: Request,
                      username: str = Form('some'),
                      password: str = Form('some')):
    """Login attempt handle"""
    if is_valid(request.session, username, password):
        logger.info(f'{username} just went in')
        return RedirectResponse('/today', status_code=HTTP_302_FOUND)
    logger.info(f'Login failed with: {username = } {password = }')
    return templates.TemplateResponse('login_page.html',
                                      {'request': request,
                                       'error': 'Неверный пароль...'})


@app.get('/today', response_class=HTMLResponse)
async def today_page(request: Request):
    """Show today tasks"""
    if request.session.get('access'):
        tech_help = await TechHelpTask.get_today_tasks()
        new_employee = await NewEmployeeTask.get_today_tasks()
        dismissal = await DismissalTask.get_today_tasks()
        return templates.TemplateResponse(
            name='today.html',
            context={'request': request,
                     'tech_help': tech_help,
                     'new_employee': new_employee,
                     'dismissal': dismissal,
                     'today': now().strftime('%d.%m.%Y')}
        )
    logger.info(f'Today lookup failed for: {request.session = }')
    return HTTPException(401)


@app.get('/tasks/{_type}/{page}', response_class=HTMLResponse)
async def tasks_page(request: Request, _type: str, page: int = 1):
    """Show tasks by type and page"""
    if request.session.get('access'):
        tasks = await TASK_MAP[_type].get_tasks_by_page(offset=page - 1)
        return templates.TemplateResponse(
            name=f'{_type}.html',
            context={'request': request,
                     'tasks': tasks['tasks'],
                     'task_type': TASK_VERBOSE_MAP[_type],
                     'prev': f'/tasks/{_type}/{page - 1}'
                     if page != 1 else None,
                     'next': f'/tasks/{_type}/{page + 1}'
                     if not tasks['is_last'] else None}
        )
    logger.info(f'/tasks/{_type}/{page} lookup '
                f'failed for: {request.session = }')
    return HTTPException(401)


@app.get('/files/{task_id}/{_type}', response_class=FileResponse)
async def get_file(request: Request, task_id: int, _type: str):
    """File management"""
    if request.session.get('access'):
        user = request.session['username']
        try:
            task = await TechHelpTask.get(id=task_id)
            path = getattr(task, _type)
            assert os.path.isfile(path)
        except AssertionError:
            logger.info(f'{user} attempting to load file {path}, '
                        f'but it does not exist')
            return HTTPException(404)
        else:
            logger.info(f'{user} is loading file {path}')
            return FileResponse(path,
                                filename=path[len(f'./files/{task.id}') + 1:])
    else:
        return HTTPException(401)
