"""Admin mappings"""
from models.tasks import (NewEmployeeTask, TechHelpTask, DismissalTask)

TASK_MAP = {'new_employee': NewEmployeeTask,
            'dismissal': DismissalTask,
            'tech_help': TechHelpTask}

TASK_VERBOSE_MAP = {'new_employee': 'Новые сотрудники',
                    'dismissal': 'Увольнения',
                    'tech_help': 'Техническая помощь'}
