"""Credentials definition"""
import os

PROJECT_NAME = os.environ.get('PROJECT_NAME', 'PM BOT')
DEBUG = os.environ.get('DEBUG', True)
ALLOWED_HOSTS = os.environ.get('ALLOWED_HOSTS', '*')
REDIS_HOST = os.environ.get('REDIS_HOST', 'pm-redis')
REDIS_PORT = os.environ.get('REDIS_PORT', '6379')

postgres_creds = {
    'database': os.environ.get('POSTGRES_DB', 'bot_db'),
    'user': os.environ.get('POSTGRES_USER', 'admin'),
    'password': os.environ.get('POSTGRES_PASSWORD', 'pmRulezz>3'),
    'host': os.environ.get('POSTGRES_HOST', 'pm-db'),
    'port': os.environ.get('POSTGRES_PORT', 5432)
}

tortoise_config = {
    'connections': {
        'default': {
            'engine': 'tortoise.backends.asyncpg',
            'credentials': postgres_creds
        }
    },
    'apps': {
        'models': {
            'models': ['models.users', 'models.tasks', 'aerich.models'],
            'default_connection': 'default',
        }
    }
}
