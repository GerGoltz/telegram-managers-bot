"""Application configuration"""
from fastapi import FastAPI
from loguru import logger
from starlette.middleware.cors import CORSMiddleware
from starlette.middleware.sessions import SessionMiddleware
from starlette.staticfiles import StaticFiles
from tortoise.contrib.fastapi import register_tortoise

from .credentials import (tortoise_config, PROJECT_NAME,
                          DEBUG, ALLOWED_HOSTS)


def get_application() -> FastAPI:
    """Configure FastAPI app"""
    application = FastAPI(
        title=PROJECT_NAME,
        debug=DEBUG,
        version='1'
    )

    application.add_middleware(
        CORSMiddleware,
        allow_origins=ALLOWED_HOSTS or ['*'],
        allow_credentials=True,
        allow_methods=['*'],
        allow_headers=['*'],
    )
    application.add_middleware(
        SessionMiddleware,
        secret_key='secret'
    )
    application.mount('/static',
                      StaticFiles(directory='static'),
                      name='static')
    try:
        register_tortoise(
            application,
            config=tortoise_config,
            add_exception_handlers=True
        )
    except Exception as error:  # pylint: disable=W0703
        credentials = '\n'.join(f'{key}: {value}' for key, value
                                in tortoise_config['connections']
                                ['default']['credentials'].items())
        logger.log('ERROR', f'Problem with connection to database. \n'
                            f'Text of error: {error}\n'
                            f'Credentials: {credentials}')

    return application
