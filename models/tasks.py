"""Tasks Tortoise ORM models"""
import datetime

from tortoise import models, fields
from tortoise.timezone import now


class BaseTask(models.Model):
    """Base task model"""
    created_at = fields.DatetimeField(auto_now_add=True)
    from_user_id = fields.CharField(max_length=128)
    from_user_name = fields.CharField(max_length=128)
    is_done = fields.BooleanField(default=False)
    successor = fields.ForeignKeyField('models.TaskReceiver',
                                       on_delete=fields.SET_NULL, null=True)
    closed_at = fields.DatetimeField(null=True, default=None)

    @classmethod
    async def get_tasks_by_page(cls, limit: int = 20, offset: int = 0):
        """Used in tasks endpoint"""
        return {'tasks': await (cls.all().order_by('-created_at')
                                .limit(limit).offset(offset * limit)
                                .prefetch_related('successor')),
                'is_last': await (cls.all().order_by('-created_at')
                                  .limit(limit).offset((offset + 1) * limit)
                                  .exists())}

    @classmethod
    async def get_today_tasks(cls):
        """Used in /today endpoint"""
        return await (cls.filter(created_at__gte=now().date())
                      .prefetch_related('successor'))

    def created_time(self):
        """Pleasant time view"""
        return (self.created_at
                + datetime.timedelta(hours=3)).strftime('%H:%M')

    def closed_time(self):
        """Pleasant time view"""
        if self.closed_at:
            return (self.closed_at
                    + datetime.timedelta(hours=3)).strftime('%H:%M')
        else:
            return ' '

    class Meta:
        """This model is abstract parent"""
        abstract = True


class NewEmployeeTask(BaseTask):
    """New employee task model"""
    employee_fullname = fields.CharField(max_length=128, null=True)
    date = fields.DatetimeField()
    position = fields.CharField(max_length=128)
    mentor = fields.CharField(max_length=128)
    required = fields.TextField()
    status = fields.CharField(max_length=16)

    def date_for_human(self):
        """Pleasant date view"""
        if self.date:
            return (self.date
                    + datetime.timedelta(hours=3)).strftime('%d/%m/%Y %H:%M')
        else:
            return ' '

    class Meta:
        """Define db table related to this model"""
        table = 'new_employee_tasks'


class TechHelpTask(BaseTask):
    """Technical help task model"""
    initiator_fullname = fields.CharField(max_length=128)
    task_type = fields.CharField(max_length=128)
    description = fields.TextField()
    source_path = fields.CharField(max_length=256, null=True)
    solution_path = fields.CharField(max_length=256, null=True)

    def maybe_file(self, value: str, _type: str):
        """Return url used to get file"""
        try:
            assert value.startswith('./files/')
        except (TypeError, AssertionError):
            return value
        else:
            file_name = value[len(f'./files/{self.id}') + 1:]
            link = f'files/{self.id}/{_type}'
            return f'<a href="{link}">{file_name}</a>'

    def source(self):
        """Show source link or file"""
        return self.maybe_file(self.source_path, 'source_path')

    def solution(self):
        """Show solution link or file"""
        return self.maybe_file(self.solution_path, 'solution_path')

    class Meta:
        """Define db table related to this model"""
        table = 'tech_help_tasks'


class DismissalTask(BaseTask):
    """Dismissal task model"""
    initiator_fullname = fields.CharField(max_length=128)
    date = fields.DatetimeField()
    dismissed_fullname = fields.CharField(max_length=128)
    equipment = fields.CharField(max_length=256)

    def date_for_human(self):
        """Pleasant date view"""
        if self.date:
            return (self.date
                    + datetime.timedelta(hours=3)).strftime('%d/%m/%Y')
        else:
            return ' '

    class Meta:
        """Define db table related to this model"""
        table = 'dismissal_tasks'
