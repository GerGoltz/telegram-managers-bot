"""User related models"""
from tortoise import models, fields


class TaskReceiver(models.Model):
    """Task receiver model"""
    user_id = fields.CharField(max_length=128, unique=True)
    user_name = fields.CharField(max_length=128)

    class Meta:
        """Define db table name"""
        table = 'task_receivers'
