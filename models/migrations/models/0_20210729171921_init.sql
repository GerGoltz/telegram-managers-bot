-- upgrade --
CREATE TABLE IF NOT EXISTS "task_receivers" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "user_id" VARCHAR(128) NOT NULL UNIQUE,
    "user_name" VARCHAR(128) NOT NULL
);
CREATE TABLE IF NOT EXISTS "new_employee_tasks" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "created_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "from_user_id" VARCHAR(128) NOT NULL,
    "from_user_name" VARCHAR(128) NOT NULL,
    "is_done" BOOL NOT NULL  DEFAULT False,
    "closed_at" TIMESTAMPTZ,
    "date" TIMESTAMPTZ NOT NULL,
    "position" VARCHAR(128) NOT NULL,
    "mentor" VARCHAR(128) NOT NULL,
    "required" TEXT NOT NULL,
    "status" VARCHAR(16) NOT NULL,
    "successor_id" INT NOT NULL REFERENCES "task_receivers" ("id") ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS "aerich" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "version" VARCHAR(255) NOT NULL,
    "app" VARCHAR(20) NOT NULL,
    "content" JSONB NOT NULL
);
