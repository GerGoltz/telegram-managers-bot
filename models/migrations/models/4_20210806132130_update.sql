-- upgrade --
ALTER TABLE "new_employee_tasks" ADD "employee_fullname" VARCHAR(128);
-- downgrade --
ALTER TABLE "new_employee_tasks" DROP COLUMN "employee_fullname";
