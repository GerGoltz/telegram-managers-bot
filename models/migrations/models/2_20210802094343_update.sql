-- upgrade --
CREATE TABLE IF NOT EXISTS "dismissal_tasks" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "created_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "from_user_id" VARCHAR(128) NOT NULL,
    "from_user_name" VARCHAR(128) NOT NULL,
    "is_done" BOOL NOT NULL  DEFAULT False,
    "closed_at" TIMESTAMPTZ,
    "initiator_fullname" VARCHAR(128) NOT NULL,
    "date" TIMESTAMPTZ NOT NULL,
    "dismissed_fullname" VARCHAR(128) NOT NULL,
    "equipment" VARCHAR(256) NOT NULL,
    "successor_id" INT REFERENCES "task_receivers" ("id") ON DELETE SET NULL
);
COMMENT ON TABLE "dismissal_tasks" IS 'Dismissal task model';
-- downgrade --
DROP TABLE IF EXISTS "dismissal_tasks";
