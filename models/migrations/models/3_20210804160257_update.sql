-- upgrade --
CREATE TABLE IF NOT EXISTS "tech_help_tasks" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "created_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "from_user_id" VARCHAR(128) NOT NULL,
    "from_user_name" VARCHAR(128) NOT NULL,
    "is_done" BOOL NOT NULL  DEFAULT False,
    "closed_at" TIMESTAMPTZ,
    "initiator_fullname" VARCHAR(128) NOT NULL,
    "task_type" VARCHAR(128) NOT NULL,
    "description" TEXT NOT NULL,
    "source_path" VARCHAR(256),
    "solution_path" VARCHAR(256),
    "successor_id" INT REFERENCES "task_receivers" ("id") ON DELETE SET NULL
);
COMMENT ON TABLE "tech_help_tasks" IS 'Technical help task model';
-- downgrade --
DROP TABLE IF EXISTS "tech_help_tasks";
