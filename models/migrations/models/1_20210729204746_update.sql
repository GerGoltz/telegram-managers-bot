-- upgrade --
ALTER TABLE "new_employee_tasks" ALTER COLUMN "successor_id" DROP NOT NULL;
-- downgrade --
ALTER TABLE "new_employee_tasks" ALTER COLUMN "successor_id" SET NOT NULL;
