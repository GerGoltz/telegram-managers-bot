"""Mappings for models"""
from models.tasks import (NewEmployeeTask,
                          TechHelpTask,
                          DismissalTask)
from utils.texts import (NEW_EMPLOYEE_LITER,
                         TECH_HELP_LITER,
                         DISMISSAL_LITER)

TASK_LITER_MAP = {NEW_EMPLOYEE_LITER: NewEmployeeTask,
                  TECH_HELP_LITER: TechHelpTask,
                  DISMISSAL_LITER: DismissalTask}
