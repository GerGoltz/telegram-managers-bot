"""Project's configuration"""
import os

redis_creds = {'host': os.environ.get('REDIS_HOST', 'pm-redis'),
               'port': os.environ.get('REDIS_PORT', '6379')}

postgres_creds = {
    'database': os.environ.get('BACKOFFICE_DB_NAME', 'bot_db'),
    'user': os.environ.get('BACKOFFICE_USER_DB', 'admin'),
    'password': os.environ.get('BACKOFFICE_PASSWORD_DB', 'pmRulezz>3'),
    'host': os.environ.get('BACKOFFICE_HOST_DB', 'pm-db'),
    'port': os.environ.get('BACKOFFICE_PORT_DB', 5432)
}

tortoise_config = {
    'connections': {
        'default': {
            'engine': 'tortoise.backends.asyncpg',
            'credentials': postgres_creds
        }
    },
    'apps': {
        'models': {
            'models': ['models.users', 'models.tasks', 'aerich.models'],
            'default_connection': 'default',
        }
    }
}
BOT_TOKEN = os.getenv('BOT_TOKEN',
                      default='1054632339:AAGos_Z548D4JJHjtm_3LnF0oY17Hcdf-aw')
SECRET_PM_WORD = os.getenv('SECRET_PM_WORD',
                           default='Кукарача дико скачет')
