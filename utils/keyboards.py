"""Common keyboards"""
from aiogram.types import ReplyKeyboardMarkup, KeyboardButton

from utils.texts import (NEW_EMPLOYEE_TEXT, CANCEL_TEXT,
                         TECH_HELP_TEXT, DISMISSAL_TEXT)

choices_keyboard = ReplyKeyboardMarkup(
    resize_keyboard=True,
    keyboard=[[KeyboardButton(NEW_EMPLOYEE_TEXT)],
              [KeyboardButton(TECH_HELP_TEXT)],
              [KeyboardButton(DISMISSAL_TEXT)]]
)
cancel_keyboard = ReplyKeyboardMarkup(
    resize_keyboard=True,
    keyboard=[[KeyboardButton(CANCEL_TEXT)]]
)
