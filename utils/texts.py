"""Texts used in bot"""
CHOICE_ORDER_TEXT = ('Чтобы оставить заявку: '
                     'нажми на кнопку на клавиатуре ниже')
START_BOT_TEXT = 'Приветствуем в Бастионе 👌\n' + CHOICE_ORDER_TEXT
SECRET_RESPONSE = 'Поздравяем! Теперь вы участвуете в обработке заявок.'
SECRET_DOUBLE_TEXT = 'Вы уже в клубе'
CANCEL_TEXT = 'Отмена'
ERROR_HINT_TEXT = ('Что-то пошло не так...\n'
                   'Отправьте еще раз.\n\n'
                   '<i>Подсказка:\n{}</i>')
FINISH_TEXT = 'Ваша заявка №{} принята в работу.'
SUCCESS_TEXT = 'Готово'
ORDER_CLOSE_TEXT = '<b>Заявка №{} выполнена!</b>'
GOOD_JOB_TEXT = 'Молодчик!'

# New employee case texts
NEW_EMPLOYEE_LITER = 'A'
NEW_EMPLOYEE_TEXT = 'Заявка на нового сотрудника'
NEW_EMPLOYEE_FULLNAME_TEXT = 'Введите ФИО нового сотрудника'
NEW_EMPLOYEE_DATE_TEXT = '<b>Дата выхода</b>\n\n<i>DD.MM.YYYY</i>'
NEW_EMPLOYEE_TIME_TEXT = '<b>Время выхода сотрудника</b>\n\n<i>hh:mm</i>'
NEW_EMPLOYEE_POSITION_TEXT = '<b>Должность</b>'
NEW_EMPLOYEE_MENTOR_TEXT = ('<b>Наставник</b>\n\n'
                            '<i>ИО ответственного сотрудника</i>')
NEW_EMPLOYEE_REQUIRED_TEXT = ('<b>Что необходимо?</b>\n\nПеречень оборудования'
                              '/программ необходимых для установки '
                              'новому сотруднику \n\n'
                              '<i>(ноутбук, телефон, почта, '
                              'телеграмм / ноутбук, телеграмм)</i>')
NEW_EMPLOYEE_STATUS_TEXT = ('<b>Статус сотрудника</b>\n\n'
                            'Выбери из вариантов ниже:')
NEW_EMPLOYEE_STATUS = {'test': 'Тестовые дни',
                       'state': 'В штат'}

NEW_EMPLOYEE_VERBOSE_MAPPING = {
    'employee_fullname': 'ФИО нового сотрудника',
    'date': 'Дата',
    'time': 'Время',
    'position': 'Должность',
    'mentor': 'Наставник',
    'required': 'Необходимо для выхода',
    'status': 'Статус'
}

# Tech help case texts
TECH_HELP_LITER = 'B'
TECH_HELP_TEXT = 'Техническая помощь'
TECH_HELP_INITIATOR_FULLNAME_TEXT = '<b>ФИО инициатора заявки</b>'
TECH_HELP_TASK_TYPE_TEXT = ('<b>Тип заявки</b>\n\n'
                            'Выбери из вариантов ниже:')
TECH_HELP_TASK_TYPE_MAPPING = {
    'a': 'Помощь в работе орг. техники',
    'b': 'Помощь с документами',
    'c': 'Помощь с установленным ПО',
    'd': 'Помощь в установке ПО',
    'e': 'Замена орг. техники',
    'f': 'Разработка визуального оформления',
    'g': 'Другое',
}
NO_FILE_CASE = ('a', 'c', 'd', 'e')
TECH_HELP_DESCRIPTION_TEXT = 'Опишите задачу'
DESCRIPTION_NEXT_STATE_MAP = {
    key: 'Check' if key in NO_FILE_CASE else 'File'
    for key in TECH_HELP_TASK_TYPE_MAPPING
}
TECH_HELP_FILE_TEXT = ('Отправьте файл или ссылку\n\n '
                       '<i>Либо оставьте комментарий, '
                       'чтобы продолжить без файла</i>')
FILE_CAPTION = 'Во вложении'
FILE_DESTINATION = './files/{}'
TECH_HELP_VERBOSE_MAPPING = {
    'initiator_fullname': 'Инициатор',
    'task_type': 'Тип задачи',
    'description': 'Описание',
    'source_path': 'Файл или ссылка'
}
TECH_HELP_FILE_READY_TEXT = 'Файл готов'
TECH_HELP_FILE_RECEIVE_TEXT = 'Пришлите файл-решение'
TECH_TASK_DONE_CAPTION = 'Ваш файл-решение'

# Dismissal case texts
DISMISSAL_LITER = 'C'
DISMISSAL_TEXT = 'Заявка на увольнение'
DISMISSAL_INITIATOR_FULLNAME_TEXT = '<b>ФИО инициатора заявки</b>'
DISMISSAL_DATE_TEXT = '<b>Дата увольнения</b>\n\n<i>DD.MM.YYYY</i>'
DISMISSAL_DISMISSED_FULLNAME_TEXT = '<b>ФИО уволенного сотрудника</b>'
DISMISSAL_EQUIPMENT_TEXT = ('<b>Оборудование</b>\n\n'
                            '<i>информация о выданном оборудовании, '
                            'подлежащим возврату</i>')
DISMISSAL_VERBOSE_MAPPING = {
    'initiator_fullname': 'Инициатор',
    'date': 'Дата',
    'dismissed_fullname': 'Уволенный сотрудник',
    'equipment': 'Оборудование, подлежащее возврату'
}

#  States hint texts
BOT_EXPECTS_INLINE_TEXT = 'Нажмите на нужную кнопку под сообщением.'
CHECK_HINT_TEXT = 'Подтвердите заявку или составьте заново.'
MAX_LENGTH_VALIDATION_TEXT = 'Попробуйте более кратко (менее {} символов)'
DATE_VALIDATION_TEXT = 'Формат: ДД.ММ.ГГГГ'
TIME_VALIDATION_TEXT = 'Формат: чч:мм'
TYPE_HINT_TEXT = 'Пришлите документ, фотографию или видеофайл'
