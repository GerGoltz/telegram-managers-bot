"""States to pass through"""
from aiogram import Dispatcher
from aiogram.dispatcher.filters.state import State, StatesGroup

from utils.texts import (BOT_EXPECTS_INLINE_TEXT, CHECK_HINT_TEXT,
                         MAX_LENGTH_VALIDATION_TEXT, DATE_VALIDATION_TEXT,
                         TIME_VALIDATION_TEXT, TYPE_HINT_TEXT)


class HintedState(State):
    """Base states but with hints"""
    def __init__(self, hint: str = None, *args):
        """Added hint as attribute"""
        super().__init__(*args)
        self.hint = hint

    async def set(self, user=None):
        """Option to set state for concrete user"""
        state = Dispatcher.get_current().current_state(user=user)
        await state.set_state(self.state)


class NewEmployeeApplication(StatesGroup):
    """New Employee Application state machine"""
    EmployeeFullname = HintedState(MAX_LENGTH_VALIDATION_TEXT.format(128))
    Date = HintedState(DATE_VALIDATION_TEXT)
    Time = HintedState(TIME_VALIDATION_TEXT)
    Position = HintedState(MAX_LENGTH_VALIDATION_TEXT.format(128))
    Mentor = HintedState(MAX_LENGTH_VALIDATION_TEXT.format(128))
    Required = HintedState()
    Status = HintedState(BOT_EXPECTS_INLINE_TEXT)


class TechHelpApplication(StatesGroup):
    """Tech Help Application state machine"""
    InitiatorFullname = HintedState(MAX_LENGTH_VALIDATION_TEXT.format(128))
    TaskType = HintedState(BOT_EXPECTS_INLINE_TEXT)
    Description = HintedState()
    File = HintedState(TYPE_HINT_TEXT)
    Check = HintedState(CHECK_HINT_TEXT)
    Active = HintedState(TYPE_HINT_TEXT)


class DismissalApplication(StatesGroup):
    """Dismissal Application state machine"""
    InitiatorFullname = HintedState(MAX_LENGTH_VALIDATION_TEXT.format(128))
    Date = HintedState(DATE_VALIDATION_TEXT)
    DismissedFullname = HintedState(MAX_LENGTH_VALIDATION_TEXT.format(128))
    Equipment = HintedState(MAX_LENGTH_VALIDATION_TEXT.format(256))
    Check = HintedState(CHECK_HINT_TEXT)
