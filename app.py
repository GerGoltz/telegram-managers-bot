"""Start file of app"""
# !/usr/bin/env python
from aiogram.utils.executor import start_polling
from loguru import logger
from tortoise import Tortoise

import middlewares
from config import tortoise_config
from handlers import dp
from utils.logging import setup as logging_setup


async def on_startup(dispatcher):
    middlewares.setup(dispatcher)
    await Tortoise.init(config=tortoise_config)
    logger.info('Tortoise-ORM started, '
                f'{Tortoise._connections["default"].__dict__}, '
                f'{Tortoise.apps}')
    # logger.info('Tortoise-ORM generating schema')
    # await Tortoise.generate_schemas()


async def on_shutdown(dispatcher):
    await Tortoise.close_connections()
    logger.info('Tortoise-ORM shutdown')

if __name__ == '__main__':
    logging_setup()
    start_polling(dispatcher=dp,
                  on_startup=on_startup,
                  on_shutdown=on_shutdown,
                  skip_updates=True)
